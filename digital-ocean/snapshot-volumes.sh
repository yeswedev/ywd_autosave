#!/bin/sh
set -o errexit

LIBRARY="$(dirname "$0")/../libautosave.sh"
if [ ! -e "$LIBRARY" ]; then
	printf '\nError:\n' >&2
	printf 'The library could not be found.\n' >&2
	printf 'Did you forget to build it?\n' >&2
	exit 1
fi
# shellcheck source=libautosave.sh
. "$LIBRARY"

configuration_file=$(realpath "$(dirname "$0")/../config/digital-ocean.conf")
if [ ! -f "$configuration_file" ]; then
	printf '\nError:\n' >&2
	printf 'Required configuration file could not be found:\n%s\n' "$configuration_file" >&2
	exit 1
fi
# shellcheck source=config/digital-ocean.conf.example
. "$configuration_file"

if [ -z "$DIGITALOCEAN_ACCESS_TOKEN" ]; then
	printf '\nError:\n' >&2
	printf 'Required configuration value is not set:\n%s\n' 'DIGITALOCEAN_ACCESS_TOKEN' >&2
	printf 'See example configuration file for required values:\n%s\n' "$configuration_file.example" >&2
	exit 1
fi

digitalocean_authenticate "$DIGITALOCEAN_ACCESS_TOKEN"
volumes_list=$(digitalocean_volumes_list)
# shellcheck disable=SC2086
digitalocean_volumes_snapshot $volumes_list

exit 0
