#!/bin/sh
set -o errexit

ROOT_DIR="$(readlink --canonicalize "$(dirname "$0")/..")"

(
	cd "$ROOT_DIR"
	for shell in sh bash dash ksh; do
		printf 'Testing code syntax in %s mode…\n' "$shell"
		shellcheck --shell="$shell" tests/*.sh
		shellcheck --shell="$shell" --external-sources lib/*.sh
		shellcheck --shell="$shell" --external-sources quicksave.sh cloudsave.sh autosave.sh
		shellcheck --shell="$shell" --external-sources digital-ocean/snapshot-volumes.sh
	done
)

exit 0
