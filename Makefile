all: libautosave.sh

libautosave.sh: lib/*
	cat lib/* > libautosave.sh

clean:
	rm -f libautosave.sh
