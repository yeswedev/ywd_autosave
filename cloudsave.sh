#!/bin/sh
set -o errexit

LIBRARY="$(realpath "$(dirname "$0")")/libautosave.sh"
if [ ! -e "$LIBRARY" ]; then
	printf '\nError:\n' >&2
	printf 'The library could not be found.\n' >&2
	printf 'Did you forget to build it?\n' >&2
	exit 1
fi
# shellcheck source=libautosave.sh
. "$LIBRARY"

for configuration_file in \
	"$(realpath "$(dirname "$0")")/config/cloudsave.conf" \
	"$(realpath "$(dirname "$0")")/config/quicksave.conf"
do
	if [ -f "$configuration_file" ]; then
		# shellcheck source=config/digital-ocean.conf.example
		. "$configuration_file"
	else
		printf '\nError:\n' >&2
		printf 'Required configuration file could not be found:\n%s\n' "$configuration_file" >&2
		exit 1
	fi
done

ssh_key="$(realpath "$(dirname "$0")")/ssh/id_rsa"
if [ ! -f "$ssh_key" ]; then
	printf '\nError:\n' >&2
	printf 'Expected SSH private key is missing:\n%s\n' "$ssh_key" >&2
	exit 1
fi

if ! command -v rsync >/dev/null 2>/dev/null; then
	printf '\nError:\n' >&2
	printf 'Command not found: %s\n' 'rsync' >&2
	exit 1
fi

rsync --partial --recursive --times --rsh="ssh -i '$ssh_key'" "$BACKUPS_PATH/" "$SERVER_USER@$SERVER_HOSTNAME:$SERVER_BACKUPS_PATH/"

exit 0
