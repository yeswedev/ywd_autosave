#!/bin/sh
set -o errexit

APP_DIR=$(dirname "$(realpath "$0")")

LIBRARY="$APP_DIR/libautosave.sh"
if [ ! -e "$LIBRARY" ]; then
	printf 'Error:\n' >&2
	printf 'Required library not found: %s\n' "$LIBRARY" >&2
	printf 'You can build it running: %s\n' "cd '$APP_DIR' && make" >&2
	exit 1
fi
# shellcheck source=libautosave.sh
. "$LIBRARY"

if [ $# -lt 1 ]; then
	# shellcheck disable=SC2016
	printf 'USAGE: %s $project_path[…]\n' "$(basename "$0")"
	exit 1
fi

parse_configuration

EXIT_STATUS=0
for PROJECT_PATH in "$@"; do
	if get_project_db_access_infos "$PROJECT_PATH"; then
		generate_dump "$PROJECT_PATH"
	else
		printf '\nError:\n' >&2
		printf 'Could not get database access informations for "%s"\n' "$(realpath "$PROJECT_PATH")" >&2
		EXIT_STATUS=1
	fi
done

exit $EXIT_STATUS
