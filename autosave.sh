#!/bin/sh
set -o errexit

BASE_PATH=$(realpath "$(dirname "$0")")

LIBRARY="$BASE_PATH/libautosave.sh"
if [ ! -e "$LIBRARY" ]; then
	printf '\nError:\n' >&2
	printf 'The library could not be found.\n' >&2
	printf 'Did you forget to build it?\n' >&2
	exit 1
fi
# shellcheck source=libautosave.sh
. "$LIBRARY"

projects_list="$BASE_PATH/config/projects.list"
if [ ! -f "$projects_list" ]; then
	printf '\nError:\n' >&2
	printf 'Required configuration file could not be found:\n%s\n' "$projects_list" >&2
	exit 1
fi

while read -r project_path; do
	"$BASE_PATH/quicksave.sh" "$project_path"
done << EOL
$(cat "$projects_list")
EOL

"$BASE_PATH/cloudsave.sh"

exit 0
