get_project_db_access_infos_prestashop() {
	if [ $# -ne 1 ]; then
		# shellcheck disable=SC2016
		printf 'USAGE: get_project_db_access_infos_prestashop $project_path\n'
		return 1
	fi
	# shellcheck disable=SC2039
	local project_path conf_file
	project_path="$1"
	conf_file="$project_path/app/config/parameters.php"
	if [ ! -e "$conf_file" ]; then
		error_file_not_found "$conf_file"
		return 1
	fi
	DB_HOST=$(grep "\\s*'database_host' => '.\\+'," "$conf_file" | sed "s/\\s*'database_host' => '\\(.\\+\\)',/\\1/")
	DB_NAME=$(grep "\\s*'database_name' => '.\\+'," "$conf_file" | sed "s/\\s*'database_name' => '\\(.\\+\\)',/\\1/")
	DB_USER=$(grep "\\s*'database_user' => '.\\+'," "$conf_file" | sed "s/\\s*'database_user' => '\\(.\\+\\)',/\\1/")
	DB_PASS=$(grep "\\s*'database_password' => '.\\+'," "$conf_file" | sed "s/\\s*'database_password' => '\\(.\\+\\)',/\\1/")
	export DB_HOST DB_NAME DB_USER DB_PASS
	return 0
}

get_project_db_access_infos_prestashop16() {
	if [ $# -ne 1 ]; then
		# shellcheck disable=SC2016
		printf 'USAGE: get_project_db_access_infos_prestashop16 $project_path\n'
		return 1
	fi
	# shellcheck disable=SC2039
	local project_path conf_file
	project_path="$1"
	conf_file="$project_path/config/settings.inc.php"
	if [ ! -e "$conf_file" ]; then
		error_file_not_found "$conf_file"
		return 1
	fi
	DB_HOST=$(grep "define('_DB_SERVER_', '.\\+');" "$conf_file" | sed "s/define('_DB_SERVER_', '\\(.\\+\\)');/\\1/")
	DB_NAME=$(grep "define('_DB_NAME_', '.\\+');" "$conf_file" | sed "s/define('_DB_NAME_', '\\(.\\+\\)');/\\1/")
	DB_USER=$(grep "define('_DB_USER_', '.\\+');" "$conf_file" | sed "s/define('_DB_USER_', '\\(.\\+\\)');/\\1/")
	DB_PASS=$(grep "define('_DB_PASSWD_', '.\\+');" "$conf_file" | sed "s/define('_DB_PASSWD_', '\\(.\\+\\)');/\\1/")
	export DB_HOST DB_NAME DB_USER DB_PASS
	return 0
}
