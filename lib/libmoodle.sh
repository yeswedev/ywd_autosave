get_project_db_access_infos_moodle() {
	if [ $# -ne 1 ]; then
		# shellcheck disable=SC2016
		printf 'USAGE: get_project_db_access_infos_moodle $project_path\n'
		return 1
	fi
	# shellcheck disable=SC2039
	local project_path conf_file
	project_path="$1"
	conf_file="$project_path/config.php"
	if [ ! -e "$conf_file" ]; then
		error_file_not_found "$conf_file"
		return 1
	fi
	DB_HOST=$(sed --silent "/^\$CFG->dbhost/s/^\$CFG->dbhost\\s*= '\\(.*\\)';.*$/\\1/p" "$conf_file")
	DB_NAME=$(sed --silent "/^\$CFG->dbname/s/^\$CFG->dbname\\s*= '\\(.*\\)';.*$/\\1/p" "$conf_file")
	DB_USER=$(sed --silent "/^\$CFG->dbuser/s/^\$CFG->dbuser\\s*= '\\(.*\\)';.*$/\\1/p" "$conf_file")
	DB_PASS=$(sed --silent "/^\$CFG->dbpass/s/^\$CFG->dbpass\\s*= '\\(.*\\)';.*$/\\1/p" "$conf_file")
	export DB_HOST DB_NAME DB_USER DB_PASS
	return 0
}
