mongodb_get_database_url() {
	if [ $# -ne 1 ]; then
		printf 'USAGE: mongodb_get_database_url FILE\n' >&2
		return 1
	fi
	# shellcheck disable=SC2039
	local conf_file database_url
	conf_file="$1"
	database_url=$(grep 'mongodb://' "$conf_file" | \
		sed "s#.*uri: .*'\\(mongodb://.*\\)'.*#\\1#;s#' + (process.env.DB_1_PORT_27017_TCP_ADDR || '\\(.*\\)') + '#\\1#")
	printf '%s' "$database_url"
	return 0
}
mongodb_get_database_host() {
	if [ $# -ne 1 ]; then
		printf 'USAGE: mongodb_get_database_host FILE\n' >&2
		return 1
	fi
	# shellcheck disable=SC2039
	local conf_file database_url database_host
	conf_file="$1"
	database_url=$(mongodb_get_database_url "$conf_file")
	database_host=$(printf '%s' "$database_url" | sed 's#mongodb://\(.*\)/.*#\1#')
	printf '%s' "$database_host"
	return 0
}
mongodb_get_database_name() {
	if [ $# -ne 1 ]; then
		printf 'USAGE: mongodb_get_database_name FILE\n' >&2
		return 1
	fi
	# shellcheck disable=SC2039
	local conf_file database_url database_name
	conf_file="$1"
	database_url=$(mongodb_get_database_url "$conf_file")
	database_name=$(printf '%s' "$database_url" | sed 's#mongodb://.*/\(.*\)#\1#')
	printf '%s' "$database_name"
	return 0
}
get_project_db_access_infos_mongodb() {
	if [ $# -ne 1 ]; then
		printf 'USAGE: get_project_db_access_infos_mongodb PROJECT_PATH\n'
		return 1
	fi
	unset DB_LIST
	export MULTIPLE_DB=1
	# shellcheck disable=SC2039
	local project_path conf_file env_name
	project_path="$1"
	for conf_file in "$project_path"/config/env/*.js; do
		env_name=$(basename "$conf_file" '.js' | sed 's/-/_/g')
		if {
			[ -z "${env_name##*.example}" ] || \
			! grep --silent 'mongodb://' "$conf_file"
		}; then
			continue
		fi
		if [ -z "$DB_LIST" ]; then
			DB_LIST="$env_name"
		else
			DB_LIST="$DB_LIST $env_name"
		fi
		eval "$(printf "%s='%s'" "DB_HOST_${env_name}" "$(mongodb_get_database_host "$conf_file")")"
		eval "$(printf "%s='%s'" "DB_NAME_${env_name}" "$(mongodb_get_database_name "$conf_file")")"
		# shellcheck disable=SC2086
		export DB_HOST_${env_name} DB_NAME_${env_name}
	done
	export DB_LIST
	return 0
}
generate_dump_multiple_mongodb() {
	# shellcheck disable=SC2039
	local env_name db_name db_host dump_name
	for env_name in "$@"; do
		for variable in DB_HOST_${env_name} DB_NAME_${env_name}; do
			value=$(eval printf -- '%b' \"\$"$variable"\")
			if [ -z "$value" ]; then
				error_variable_not_set "$variable"
				return 1
			fi
		done
		mkdir --parents "$BACKUPS_PATH"
		db_name=$(eval printf -- '%b' \"\$"DB_NAME_${env_name}"\")
		db_host=$(eval printf -- '%b' \"\$"DB_HOST_${env_name}"\")
		dump_name="$db_name.$(date +%F-%H:%M)"
		mongodump --host "$db_host" --db "$db_name"
		if [ -d "dump/$db_name" ]; then
			rmdir --ignore-fail-on-non-empty "dump/$db_name"
			if [ -d "dump/$db_name" ]; then
				tar czf "$BACKUPS_PATH/$dump_name.tar.gz" "dump/$db_name"
				rm --recursive "dump/$db_name"
				rmdir --ignore-fail-on-non-empty 'dump'
			fi
		fi
		delete_older_dumps "$db_name"
	done
}
