set_debug() {
	if [ -z "$DEBUG" ]; then
		export DEBUG=0
	fi
}

get_project_type() {
	set_debug
	if [ $# -ne 1 ]; then
		# shellcheck disable=SC2016
		printf 'USAGE: get_project_type $project_path\n'
		return 1
	fi
	YWD_MONITOR_DIR="$APP_DIR/ywd_monitor"
	if [ ! -e "$YWD_MONITOR_DIR/Makefile" ]; then
		(
			cd "$APP_DIR" || {
				printf 'Critical error:\n' >&2
				printf 'Couldnʼt enter the following directory: %s\n' "$APP_DIR" >&2
				printf 'Please report this issue on our issues tracker: %s\n' 'https://framagit.org/yeswedev/ywd_autosave/issues' >&2
				return 1
			}
			git submodule update --init --recursive ywd_monitor >/dev/null
		)
	fi
	if [ ! -e "$YWD_MONITOR_DIR/libmonitor.sh" ]; then
		(
			cd "$YWD_MONITOR_DIR" || {
				printf 'Critical error:\n' >&2
				printf 'Couldnʼt enter the following directory: %s\n' "$YWD_MONITOR_DIR" >&2
				printf 'Please report this issue on our issues tracker: %s\n' 'https://framagit.org/yeswedev/ywd_autosave/issues' >&2
				return 1
			}
			make >/dev/null
		)
	fi
	# shellcheck disable=SC2039
	local project_path project_type
	project_path="$1"
	project_type=$("$YWD_MONITOR_DIR/get-project-type.sh" "$project_path")
	if [ $DEBUG -eq 1 ]; then
		# shellcheck disable=SC2016
		printf '$project_type set to "%s" using informations from YWD Monitor.\n' "$project_type" >&2
	fi
	if [ "$project_type" = 'unknown' ]; then
		if [ $DEBUG -eq 1 ]; then
			printf 'falling back on built-in type detection\n' >&2
		fi
		for file in "$project_path"/config/env/*.js; do
			if [ -e "$file" ]; then
				project_type='mongodb'
			fi
		done
		if [ $DEBUG -eq 1 ]; then
			# shellcheck disable=SC2016
			printf '$project_type guessed as "%s"\n' "$project_type" >&2
		fi
	fi
	printf '%s' "$project_type"
}

get_project_db_access_infos() {
	set_debug
	if [ $# -ne 1 ]; then
		# shellcheck disable=SC2016
		printf 'USAGE: get_project_db_access_infos $project_path\n'
		return 1
	fi
	unset DB_HOST DB_NAME DB_USER DB_PASS DB_LIST
	MULTIPLE_DB=0
	# shellcheck disable=SC2039
	local project_path project_type value site_name name value
	project_path="$1"
	project_type=$(get_project_type "$project_path")
	if [ "$project_type" = 'unknown' ]; then
		printf '\nError:\n' >&2
		# shellcheck disable=SC2016
		printf '$project_type could not be guessed for "%s"\n' "$project_path" >&2
		return 1
	else
		case "$project_type" in
			('laravel')
				get_project_db_access_infos_laravel "$project_path"
			;;
			('wordpress')
				get_project_db_access_infos_wordpress "$project_path"
			;;
			('wordpress_vanilla')
				get_project_db_access_infos_wordpress_vanilla "$project_path"
			;;
			('prestashop')
				get_project_db_access_infos_prestashop "$project_path"
			;;
			('prestashop1.6')
				get_project_db_access_infos_prestashop16 "$project_path"
			;;
			('drupal')
				get_project_db_access_infos_drupal "$project_path"
			;;
			('drupal_composer')
				get_project_db_access_infos_drupal_composer "$project_path"
			;;
			('mongodb')
				get_project_db_access_infos_mongodb "$project_path"
			;;
			('moodle')
				get_project_db_access_infos_moodle "$project_path"
			;;
			(*)
				printf '\nTODO:\n' >&2
				# shellcheck disable=SC2016
				printf 'Add ability to get database access informations for $project_type="%s"\n' "$project_type" >&2
				return 1
			;;
		esac
	fi
	if [ $MULTIPLE_DB -eq 0 ]; then
		for name in DB_HOST DB_NAME DB_USER DB_PASS; do
			value=$(eval printf -- '%b' \"\$$name\")
			if [ -z "$value" ]; then
				printf '\nError:\n' >&2
				printf 'Couldnʼt get $%s value for %s\n' "$name" "$project_path" >&2
				return 1
			fi
		done
		if [ $DEBUG -eq 1 ]; then
			for name in DB_HOST DB_NAME DB_USER DB_PASS; do
				value=$(eval printf -- '%b' \"\$$name\")
				printf '$%s="%s"\n' "$name" "$value" >&2
			done
		fi
	else
		case "$project_type" in
			('drupal')
				for site_name in $DB_LIST; do
					for name in DB_HOST_${site_name} DB_NAME_${site_name} DB_USER_${site_name} DB_PASS_${site_name}; do
						value=$(eval printf -- '%b' \"\$"$name"\")
						if [ -z "$value" ]; then
							printf '\nError:\n' >&2
							printf 'Couldnʼt get $%s value for %s\n' "$name" "$project_path" >&2
							return 1
						fi
					done
					if [ $DEBUG -eq 1 ]; then
						for name in DB_HOST_${site_name} DB_NAME_${site_name} DB_USER_${site_name} DB_PASS_${site_name}; do
							value=$(eval printf -- '%b' \"\$"$name"\")
							printf '$%s="%s"\n' "$name" "$value" >&2
						done
					fi
				done
			;;
			('mongodb')
				for env_name in $DB_LIST; do
					for name in DB_HOST_${env_name} DB_NAME_${env_name}; do
						value=$(eval printf -- '%b' \"\$"$name"\")
						if [ -z "$value" ]; then
							printf '\nError:\n' >&2
							printf 'Couldnʼt get $%s value for %s\n' "$name" "$project_path" >&2
							return 1
						fi
					done
					if [ $DEBUG -eq 1 ]; then
						for name in DB_HOST_${env_name} DB_NAME_${env_name}; do
							value=$(eval printf -- '%b' \"\$"$name"\")
							printf '$%s="%s"\n' "$name" "$value" >&2
						done
					fi
				done
			;;
		esac
	fi
}

error_file_not_found() {
	if [ $# -ne 1 ]; then
		# shellcheck disable=SC2016
		printf 'USAGE: error_file_not_found $file\n'
		return 1
	fi
	# shellcheck disable=SC2039
	local file
	file="$1"
	printf '\nError:\n' >&2
	printf 'The following required file has not been found: %s\n' "$file" >&2
	return 0
}

error_variable_not_set() {
	if [ $# -ne 1 ]; then
		# shellcheck disable=SC2016
		printf 'USAGE: error_variable_not_set $variable_name\n'
		return 1
	fi
	# shellcheck disable=SC2039
	local variable
	variable="$1"
	printf '\nError:\n' >&2
	printf 'The following required variable is not set: %s\n' "$variable" >&2
	printf 'This is probably a bug in the application, please report it on our bugs tracker: %s\n' 'https://framagit.org/yeswedev/ywd_autosave/issues' >&2
	return 0
}

parse_configuration() {
	set_debug
	# shellcheck disable=SC2039
	local backups_path_override max_dumps_override config_file
	unset backups_path_override max_dumps_override
	if [ -n "$BACKUPS_PATH" ]; then
		backups_path_override="$BACKUPS_PATH"
	fi
	if [ -n "$MAX_DUMPS" ]; then
		max_dumps_override="$MAX_DUMPS"
	fi
	unset BACKUPS_PATH
	config_file="$APP_DIR/config/quicksave.conf"
	if [ -e "$config_file" ]; then
		# shellcheck source=config/quicksave.conf.example
		. "$config_file"
		export BACKUPS_PATH
		if [ $DEBUG -eq 1 ]; then
			printf 'BACKUPS_PATH="%s"\n' "$BACKUPS_PATH"
		fi
	fi
	if [ -n "$backups_path_override" ]; then
		export BACKUPS_PATH="$backups_path_override"
	fi
	if [ -n "$max_dumps_override" ]; then
		export MAX_DUMPS="$max_dumps_override"
	fi
	if \
		[ -z "$BACKUPS_PATH" ] ||\
		[ -z "$MAX_DUMPS" ]
	then
		printf '\nError:\n' >&2
		printf 'Mandatory configuration values are not set.\n' >&2
		# shellcheck disable=SC2016
		printf 'Variables $BACKUPS_PATH and $MAX_DUMPS should bet set.\n' >&2
		printf 'You can either set them as environment variables, or set them in the following file: %s\n' "$config_file" >&2
		return 1
	fi
}

generate_dump() {
	if [ $# -ne 1 ]; then
		printf 'USAGE: generate_dump PROJECT_PATH\n'
		return 1
	fi
	# shellcheck disable=SC2039
	local project_path
	project_path="$1"
	if [ -z "$MULTIPLE_DB" ]; then
		error_variable_not_set 'MULTIPLE_DB'
		return 1
	fi
	# shellcheck disable=SC2039
	local variable value dump_name
	if [ $MULTIPLE_DB -eq 0 ]; then
		for variable in DB_HOST DB_NAME DB_USER DB_PASS; do
			value=$(eval printf -- '%b' \"\$$variable\")
			if [ -z "$value" ]; then
				error_variable_not_set "$variable"
				return 1
			fi
		done
		mkdir --parents "$BACKUPS_PATH"
		# shellcheck disable=SC2153
		dump_name="$DB_NAME.$(date +%F-%H:%M)"
		# shellcheck disable=SC2153
		mysqldump --host="$DB_HOST" --user="$DB_USER" --password="$DB_PASS" "$DB_NAME" | gzip - > "$BACKUPS_PATH/$dump_name.sql.gz"
		delete_older_dumps "$DB_NAME"
	else
		if [ -z "$DB_LIST" ]; then
			error_variable_not_set 'DB_LIST'
			return 1
		fi
		case $(get_project_type "$project_path") in
			('mongodb')
				# shellcheck disable=SC2086
				generate_dump_multiple_mongodb $DB_LIST
			;;
			(*)
				# shellcheck disable=SC2086
				generate_dump_multiple_mysql $DB_LIST
			;;
		esac
	fi
}
generate_dump_multiple_mysql() {
	# shellcheck disable=SC2039
	local site_name db_name db_host db_user db_pass dump_name
	for site_name in "$@"; do
		for variable in DB_HOST_${site_name} DB_NAME_${site_name} DB_USER_${site_name} DB_PASS_${site_name}; do
			value=$(eval printf -- '%b' \"\$"$variable"\")
			if [ -z "$value" ]; then
				error_variable_not_set "$variable"
				return 1
			fi
		done
		mkdir --parents "$BACKUPS_PATH"
		db_name=$(eval printf -- '%b' \"\$"DB_NAME_${site_name}"\")
		db_host=$(eval printf -- '%b' \"\$"DB_HOST_${site_name}"\")
		db_user=$(eval printf -- '%b' \"\$"DB_USER_${site_name}"\")
		db_pass=$(eval printf -- '%b' \"\$"DB_PASS_${site_name}"\")
		dump_name="$db_name.$(date +%F-%H:%M)"
		mysqldump --host="$db_host" --user="$db_user" --password="$db_pass" "$db_name" | gzip - > "$BACKUPS_PATH/$dump_name.sql.gz"
		delete_older_dumps "$db_name"
	done
}

delete_older_dumps() {
	if [ $# -ne 1 ]; then
		# shellcheck disable=SC2016
		printf 'USAGE: delete_older_dumps $database_name\n'
		return 1
	fi
	# shellcheck disable=SC2039
	local database_name
	database_name="$1"
	find "$BACKUPS_PATH" -name "$database_name.????-??-??-??:??.sql.gz" | sort | head --lines="-$MAX_DUMPS" | xargs rm --force
	find "$BACKUPS_PATH" -name "$database_name.????-??-??-??:??.tar.gz" | sort | head --lines="-$MAX_DUMPS" | xargs rm --force
}

trim_quotes() {
	if [ $# -ne 1 ]; then
		# shellcheck disable=SC2016
		printf 'USAGE: trim_quotes $string\n' >&2
		return 1
	fi
	# shellcheck disable=SC2039
	local string
	string="$1"
	if [ "${string%\"}" != "$string" ] && [ "${string#\"}" != "$string" ]; then
		string="${string%\"}"
		string="${string#\"}"
	elif [ "${string%\'}" != "$string" ] && [ "${string#\'}" != "$string" ]; then
		string="${string%\'}"
		string="${string#\'}"
	fi
	printf '%s' "$string"
	return 0
}
