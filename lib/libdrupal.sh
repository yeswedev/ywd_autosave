get_project_db_access_infos_drupal() {
	if [ $# -ne 1 ]; then
		# shellcheck disable=SC2016
		printf 'USAGE: get_project_db_access_infos_drupal $project_path\n'
		return 1
	fi
	# shellcheck disable=SC2039
	local project_path conf_file count file
	project_path="$1"
	count=0
	for file in "$project_path/sites/"*"/settings.php"; do
		if [ -e "$file" ]; then
			count=$((count + 1))
		fi
	done
	if [ $count -eq 1 ]; then
		get_project_db_access_infos_drupal_single "$project_path"
	elif [ $count -gt 1 ]; then
		get_project_db_access_infos_drupal_multiple "$project_path"
	else
		printf '\nError:\n' >&2
		printf 'Couldnʼt get database access infos for %s\n' "$project_path" >&2
		return 1
	fi
}
get_project_db_access_infos_drupal_composer() {
	if [ $# -ne 1 ]; then
		# shellcheck disable=SC2016
		printf 'USAGE: get_project_db_access_infos_drupal_composer $project_path\n'
		return 1
	fi
	# shellcheck disable=SC2039
	local project_path conf_file count file
	project_path="$1"
	count=0
	for file in "$project_path/web/sites/"*"/settings.php"; do
		if [ -e "$file" ]; then
			count=$((count + 1))
		fi
	done
	if [ $count -eq 1 ]; then
		get_project_db_access_infos_drupal_single "$project_path/web"
	elif [ $count -gt 1 ]; then
		get_project_db_access_infos_drupal_multiple "$project_path/web"
	else
		if [ -e "$project_path/.env" ]; then
			get_project_db_access_infos_drupal_dotenv "$project_path"
		else
			printf '\nError:\n' >&2
			printf 'Couldnʼt get database access infos for %s\n' "$project_path" >&2
			return 1
		fi
	fi
}
get_project_db_access_infos_drupal_single() {
	if [ $# -ne 1 ]; then
		# shellcheck disable=SC2016
		printf 'USAGE: get_project_db_access_infos_drupal_single $project_path\n'
		return 1
	fi
	# shellcheck disable=SC2039
	local project_path conf_file
	project_path="$1"
	conf_file="$project_path/sites/default/settings.php"
	if [ ! -e "$conf_file" ]; then
		error_file_not_found "$conf_file"
		return 1
	fi
	DB_HOST=$(grep "^\\s*'host' => '.\\+'," "$conf_file" | sed "s/^\\s*'host' => '\\(.\\+\\)',/\\1/" | head --lines=1)
	DB_NAME=$(grep "^\\s*'database' => '.\\+'," "$conf_file" | sed "s/^\\s*'database' => '\\(.\\+\\)',/\\1/" | head --lines=1)
	DB_USER=$(grep "^\\s*'username' => '.\\+'," "$conf_file" | sed "s/^\\s*'username' => '\\(.\\+\\)',/\\1/" | head --lines=1)
	DB_PASS=$(grep "^\\s*'password' => '.\\+'," "$conf_file" | sed "s/^\\s*'password' => '\\(.\\+\\)',/\\1/" | head --lines=1)
	export DB_HOST DB_NAME DB_USER DB_PASS
	return 0
}
get_project_db_access_infos_drupal_multiple() {
	if [ $# -ne 1 ]; then
		# shellcheck disable=SC2016
		printf 'USAGE: get_project_db_access_infos_drupal_multiple $project_path\n'
		return 1
	fi
	unset DB_LIST
	export MULTIPLE_DB=1
	# shellcheck disable=SC2039
	local project_path site_name
	project_path="$1"
	for file in "$project_path/sites/"*"/settings.php"; do
		site_name=$(basename "$(dirname "$file")")
		if [ -z "$DB_LIST" ]; then
			DB_LIST="$site_name"
		else
			DB_LIST="$DB_LIST $site_name"
		fi
		eval "$(printf "%s='%s'" "DB_HOST_${site_name}" "$(grep "^\\s*'host' => '.\\+'," "$file" | sed "s/^\\s*'host' => '\\(.\\+\\)',/\\1/" | head --lines=1)")"
		eval "$(printf "%s='%s'" "DB_NAME_${site_name}" "$(grep "^\\s*'database' => '.\\+'," "$file" | sed "s/^\\s*'database' => '\\(.\\+\\)',/\\1/" | head --lines=1)")"
		eval "$(printf "%s='%s'" "DB_USER_${site_name}" "$(grep "^\\s*'username' => '.\\+'," "$file" | sed "s/^\\s*'username' => '\\(.\\+\\)',/\\1/" | head --lines=1)")"
		eval "$(printf "%s='%s'" "DB_PASS_${site_name}" "$(grep "^\\s*'password' => '.\\+'," "$file" | sed "s/^\\s*'password' => '\\(.\\+\\)',/\\1/" | head --lines=1)")"
		# shellcheck disable=SC2086
		export DB_HOST_${site_name} DB_NAME_${site_name} DB_USER_${site_name} DB_PASS_${site_name}
	done
	export DB_LIST
	return 0
}
get_project_db_access_infos_drupal_dotenv() {
	if [ $# -ne 1 ]; then
		# shellcheck disable=SC2016
		printf 'USAGE: get_project_db_access_infos_drupal_dotenv $project_path\n'
		return 1
	fi
	# shellcheck disable=SC2039
	local project_path env_file
	project_path="$1"
	env_file="$project_path/.env"
	if [ ! -e "$env_file" ]; then
		error_file_not_found "$env_file"
		return 1
	fi
	DB_HOST=$(sed --quiet '/^MYSQL_HOSTNAME=/s/.*=\(.\+\)/\1/p' "$env_file")
	DB_HOST=$(trim_quotes "$DB_HOST")
	DB_NAME=$(sed --quiet '/^MYSQL_DATABASE=/s/.*=\(.\+\)/\1/p' "$env_file")
	DB_NAME=$(trim_quotes "$DB_NAME")
	DB_USER=$(sed --quiet '/^MYSQL_USER=/s/.*=\(.\+\)/\1/p' "$env_file")
	DB_USER=$(trim_quotes "$DB_USER")
	DB_PASS=$(sed --quiet '/^MYSQL_PASSWORD=/s/.*=\(.\+\)/\1/p' "$env_file")
	DB_PASS=$(trim_quotes "$DB_PASS")
	export DB_HOST DB_NAME DB_USER DB_PASS
	return 0
}
