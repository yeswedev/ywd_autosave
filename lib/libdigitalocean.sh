digitalocan_check_doctl_availability() {
	if ! command -v doctl >/dev/null 2>&1; then
		printf 'Command not found: %s\n' 'doctl' >&2
		printf 'See %s for installation instructions.\n' \
			'https://github.com/digitalocean/doctl/blob/master/README.md#installing-doctl' >&2
		return 1
	fi
}
digitalocean_authenticate() {
	if [ $# -ne 1 ]; then
		printf 'USAGE: digitalocean_authenticate ACCESS_TOKEN\n' >&2
		return 1
	fi
	digitalocan_check_doctl_availability
	# shellcheck disable=SC2039
	local access_token
	access_token="$1"
	doctl auth init --access-token "$access_token" >/dev/null
	return 0
}
digitalocean_volumes_list() {
	digitalocan_check_doctl_availability
	# shellcheck disable=SC2039
	local volumes_list
	volumes_list=$(doctl compute volume list --format=ID | tail +2)
	printf '%s' "$volumes_list"
	return 0
}
digitalocean_volumes_snapshot() {
	if [ $# -eq 0 ]; then
		printf 'USAGE: digitalocean_volumes_snapshot VOLUME_ID [VOLUME_ID]…\n' >&2
		return 1
	fi
	# shellcheck disable=SC2039
	local volume
	for volume in "$@"; do
		digitalocean_volume_snapshot "$volume"
	done
	return 0
}
digitalocean_volume_snapshot() {
	if [ $# -ne 1 ]; then
		printf 'USAGE: digitalocean_volume_snapshot VOLUME_ID\n' >&2
		return 1
	fi
	digitalocan_check_doctl_availability
	# shellcheck disable=SC2039
	local volume name
	volume="$1"
	name="Snapshot triggered by YWD AutoSave on $(date +%F)"
	doctl compute volume snapshot "$volume" --snapshot-name "$name"
	return 0
}
