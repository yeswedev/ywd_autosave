get_project_db_access_infos_wordpress() {
	if [ $# -ne 1 ]; then
		# shellcheck disable=SC2016
		printf 'USAGE: get_project_db_access_infos_wordpress $project_path\n'
		return 1
	fi
	# shellcheck disable=SC2039
	local project_path env_file
	project_path="$1"
	env_file="$project_path/.env"
	if [ ! -e "$env_file" ]; then
		error_file_not_found "$env_file"
		return 1
	fi
	DB_HOST=$(grep '^DB_HOST=' "$env_file" | sed 's/^DB_HOST=\(.\+\)/\1/;s/^"//;s/"$//;'"s/^'//;s/'$//")
	DB_NAME=$(grep '^DB_NAME=' "$env_file" | sed 's/^DB_NAME=\(.\+\)/\1/;s/^"//;s/"$//;'"s/^'//;s/'$//")
	DB_USER=$(grep '^DB_USER=' "$env_file" | sed 's/^DB_USER=\(.\+\)/\1/;s/^"//;s/"$//;'"s/^'//;s/'$//")
	DB_PASS=$(grep '^DB_PASSWORD=' "$env_file" | sed 's/^DB_PASSWORD=\(.\+\)/\1/;s/^"//;s/"$//;'"s/^'//;s/'$//")
	if [ -z "$DB_HOST" ]; then
		DB_HOST='localhost'
	fi
	export DB_HOST DB_NAME DB_USER DB_PASS
	return 0
}

get_project_db_access_infos_wordpress_vanilla() {
	if [ $# -ne 1 ]; then
		# shellcheck disable=SC2016
		printf 'USAGE: get_project_db_access_infos_wordpress_vanilla $project_path\n'
		return 1
	fi
	#shellcheck disable=SC2039
	local project_path conf_file
	project_path="$1"
	conf_file="$project_path/wp-config.php"
	if [ ! -e "$conf_file" ]; then
		error_file_not_found "$conf_file"
		return 1
	fi
	DB_HOST=$(grep "define(\\s*'DB_HOST', '.\\+'\\s*);" "$conf_file" | sed "s/define(\\s*'DB_HOST', '\\(.\\+\\)'\\s*);/\\1/")
	DB_NAME=$(grep "define(\\s*'DB_NAME', '.\\+'\\s*);" "$conf_file" | sed "s/define(\\s*'DB_NAME', '\\(.\\+\\)'\\s*);/\\1/")
	DB_USER=$(grep "define(\\s*'DB_USER', '.\\+'\\s*);" "$conf_file" | sed "s/define(\\s*'DB_USER', '\\(.\\+\\)'\\s*);/\\1/")
	DB_PASS=$(grep "define(\\s*'DB_PASSWORD', '.\\+'\\s*);" "$conf_file" | sed "s/define(\\s*'DB_PASSWORD', '\\(.\\+\\)'\\s*);/\\1/")
	export DB_HOST DB_NAME DB_USER DB_PASS
	return 0
}
