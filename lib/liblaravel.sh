get_project_db_access_infos_laravel() {
	if [ $# -ne 1 ]; then
		# shellcheck disable=SC2016
		printf 'USAGE: get_project_db_access_infos_laravel $project_path\n'
		return 1
	fi
	# shellcheck disable=SC2039
	local project_path env_file
	project_path="$1"
	env_file="$project_path/.env"
	if [ ! -e "$env_file" ]; then
		error_file_not_found "$env_file"
		return 1
	fi
	DB_HOST=$(grep '^DB_HOST=' "$env_file" | sed 's/^DB_HOST=\(.\+\)/\1/;s/^"//;s/"$//;'"s/^'//;s/'$//")
	DB_NAME=$(grep '^DB_DATABASE=' "$env_file" | sed 's/^DB_DATABASE=\(.\+\)/\1/;s/^"//;s/"$//;'"s/^'//;s/'$//")
	DB_USER=$(grep '^DB_USERNAME=' "$env_file" | sed 's/^DB_USERNAME=\(.\+\)/\1/;s/^"//;s/"$//;'"s/^'//;s/'$//")
	DB_PASS=$(grep '^DB_PASSWORD=' "$env_file" | sed 's/^DB_PASSWORD=\(.\+\)/\1/;s/^"//;s/"$//;'"s/^'//;s/'$//")
	export DB_HOST DB_NAME DB_USER DB_PASS
	return 0
}
